# n01-start

Вебинар 1 по курсу Otus - Linux Security


## Homework

1. Студентам предлагается зарегистрироваться на [GitHub](https://github.com) или [GitLab](https://gitlab.com/)
2. Обновить терминальный git. Если работаете не в терминале, то [скачать и настроить Git for Linux/Win/MacOS](https://git-scm.com/downloads)
3. [Устанавливаем последнюю версию VirtualBox 6](https://www.virtualbox.org/wiki/Downloads)
4. [Устанавливаем Vagrant - https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)
5. (опционально) [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
6. Написать конфигурационный Vagrantfile, для развертывания 2-х виртуальных машин: Kali (машина атакующего) и CentOS 7 (защищаемый сервер). 
7. Сдать задание, для этого выложить ссылку на репозиторий github с вашим конфиг-файлом Vagrantfile


## Установка и настройка Vagrant для курсов ОТУС

- [Vagrant - как и для чего использовать (видео)](https://www.youtube.com/watch?time_continue=323&v=koJ1UpEOcVw)
- [Github репозиторий Леонида Альбрехта по Vagrant](https://github.com/lalbrekht/vagrant) 
- [Git howto](https://githowto.com/ru) 
- [Git branches](https://learngitbranching.js.org/)
- [Ссылка на актуальный образ Kali на Vagrant](https://app.vagrantup.com/kalilinux/boxes/rolling)
- [Ссылка на образ CentOS 7 на Vagrant](https://app.vagrantup.com/centos/boxes/7)

Продвинутый материал:
  - [Пример демостенда pacemaker c virtualbox fence](https://github.com/mbfx/otus-linux-pacemaker-vbox)
